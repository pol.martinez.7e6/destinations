import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AboutView from '../views/AboutView.vue'
import DestinationView from '../views/DestinationView.vue'
import ExperienceDetail from '../components/ExperienceDetail.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      component: AboutView
    },
    {
      path: '/destination/:id/:slug',
      name: 'destination',
      component: DestinationView,
      props: route=> ({ ...route.params, id:parseInt(route.params.id as string)}),
      children: [
        {
          path: ':exp-slug',
          component: ExperienceDetail,
          props: true
        }
      ]
    }
  ]
})

export default router